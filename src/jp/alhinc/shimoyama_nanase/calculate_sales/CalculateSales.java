package jp.alhinc.shimoyama_nanase.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		
		//コマンドライン引数のチェック
		if(args.length != 1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		
		//支店定義ファイル読み込み処理
		if(!readLstFile(args[0], "branch.lst", branchNames, branchSales)) {
				return;
			}
	
		//売上ファイル読み込み処理
		File[] files = new File(args[0]).listFiles();
	
		List<File> rcdFiles = new ArrayList<>();
	
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if (fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		 
		Collections.sort(rcdFiles);
		
		//連番チェック
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get( i + 1).getName().substring(0,8));
			
			if((latter - former) != 1 ) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		
		BufferedReader br = null;
		for (int i =0; i < rcdFiles.size(); i++) {
		try {
			br = new BufferedReader(new FileReader(rcdFiles.get(i)));
			
			
			ArrayList<String> fileContents = new ArrayList<>();
			String line = "";
			while((line = br.readLine()) != null) {
				fileContents.add(line);
			}
			
			String fileName = rcdFiles.get(i).getName();
			
			//行数チェック
			if(fileContents.size() != 2) {
				System.out.println(fileName + "のフォーマットが不正です");
				return;
			}
			
			//支店コード
			String branchCode = fileContents.get(0);
			
			//支店コードの存在確認
			if(!branchNames.containsKey(fileContents.get(0))) {
				System.out.println(fileName + "の支店コードが不正です");
				return;
			}
			
			//売上金額の数値チェック
			if (!fileContents.get(1).matches("^[0-9]+$")) {
			       System.out.println("予期せぬエラーが発生しました");
			       return;
			   } 
			
			//売上金額の加算
			long fileSale = Long.parseLong(fileContents.get(1));
			Long saleAmount = branchSales.get(branchCode) + fileSale;
			
			if(saleAmount >= 10000000000L) {
				System.out.println("合計金額が10桁を超えました");
				return;
			}
			
			
			branchSales.put(branchCode, saleAmount);

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;	
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
		

	if(!writeOutFile(args[0], "branch.out", branchNames, branchSales)) {
		return;
	}
}
	
private static boolean readLstFile
	(String filePath,
	String fileName,
	Map<String, String> branchNames,
	Map<String, Long> branchSales) {
	
		BufferedReader br = null;
	try {
	File file = new File(filePath, fileName);
	
	//支店定義ファイル存在チェック
    if (!file.exists()) {
        System.out.println("支店定義ファイルが存在しません");
        	return false;
    }
    
    
	br = new BufferedReader(new FileReader(file));
	
	
	String line;
	while((line = br.readLine()) != null) {
		String[] items = line.split(",");
		
		//支店定義ファイルフォーマットチェック
		if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
			System.out.println("支店定義ファイルのフォーマットが不正です");
			return false;
		}
		branchNames.put(items[0], items[1]);
		branchSales.put(items[0],0L);
	}
	} catch(IOException e) {
	System.out.println("予期せぬエラーが発生しました");
			return false;
	} finally {
	if(br != null) {
		try {
			br.close();
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
	}
	}
	return true;
}

private static boolean writeOutFile(
		String filePath,
		String fileName,
		Map<String, String> branchNamesMap,
		Map<String, Long> branchSalesMap
		) {

	//支店別集計ファイル書き込み処理
		BufferedWriter bw = null;
		try{
			File file = new File(filePath, fileName);
			bw =new BufferedWriter(new FileWriter(file));
			
			for(String key : branchNamesMap.keySet()) {
				bw.write( key + "," + branchNamesMap.get(key) + "," + branchSalesMap.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				}
			}
		return true;
	}
}
